<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Foundation\Auth\AuthenticatesUsers;


class LoginController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    use AuthenticatesUsers;

    public function __invoke(Request $request)
    {
        request()->validate([
            'phone_number' => 'required',
            'password' => 'required'
        ]);

        if (!$token = auth()->attempt($request->only('phone_number', 'password'))) {
            return response(null, 401);
        }

        return response()->json(compact('token'));
    }
}

<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\User;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        request()->validate([
            'name' => ['string', 'required', 'unique:users,name'],
            'email' => ['email', 'required'],
            'password' => ['required', 'min:6'],
            'phone_number' => ['required', 'unique:users,phone_number'],
            'nik' => ['required', 'integer'],
            'alamat' => ['required', 'string']
        ]);

        //dd(request('phone_number'));

        User::create([
            'name' => request('name'),
            'email' => request('email'),
            'password' => bcrypt(request('password')),
            'phone_number' => request('phone_number'),
            'nik' => request('nik'),
            'alamat' => request('alamat'),
        ]);

        return response('Terimakasih anda sudah terdaftar!!');
    }
}

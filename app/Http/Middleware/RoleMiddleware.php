<?php

namespace App\Http\Middleware;

use Closure;
use App\Log;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $route_name = \Route::current()->getName();
        $user = \Auth::user();

        if ($user->check_route($route_name)) {
            Log::create([
                'aktivitas' => $route_name,
                'status' => 'Allowed',
                'username' => $user->name,
                'user_id' => $user->id,
            ]);
            return $next($request);
        }

        Log::create([
            'aktivitas' => $route_name,
            'status' => 'Denied',
            'username' => $user->name,
            'user_id' => $user->id,
        ]);

        return response('Halaman Terlarang!!');
    }
}

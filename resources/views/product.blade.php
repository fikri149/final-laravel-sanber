<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <title></title>
    </head>
    <body>
        <div id="app">
            <h3>Halaman Product</h3>
            <input type="text">
            
            <ul>
                <li v-for="(product , index) in products">
                    <span>@{{ product.name }}</span>
                    <span>@{{ product.price }}</span>
                    <span>@{{ product.description }}</span>
                </li>
            </ul>
        </div>


    <script src="https://cdn.jsdelivr.net/npm/vue/dist/vue.js"></script>
    <script>
        new Vue({
            el:"#app",
            data:{
                newProduct : "",
                products : [
                    {id : 1, name : 'Nasgor', description : 'Coba dulu'},
                    {id : 2, name : 'Mie Goreng', description : 'Coba dulu gaes'},
                ]
            }
        });
    </script>
    </body>
</html>